# SimpleAuras
Simple aura display addon for patch 1.12

## Features
* Highly customizable
* Very lightweight, lower CPU usage than any other buff display addon, including default frames

## Screenshots
![Auras](https://i.imgur.com/K2gep4J.png)

![Config](https://i.imgur.com/1ZJrtrm.png)
