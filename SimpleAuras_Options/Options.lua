local L = AceLibrary("AceLocale-2.2"):new("SimpleAuras")
local waterfall = AceLibrary("Waterfall-1.0")

function SimpleAuras:GetOptions()
	local anchorPoints = {
		["TOPRIGHT"] = L["Top right"],
		["TOPLEFT"] = L["Top left"],
		["BOTTOMRIGHT"] = L["Bottom right"],
		["BOTTOMLEFT"] = L["Bottom left"],
		
		["TOP"] = L["Top"],
		["BOTTOM"] = L["Bottom"],
		["RIGHT"] = L["Right"],
		["LEFT"] = L["Left"],

		["CENTER"] = L["Center"],
	}
	local fontEffects = {
		["none"] = L["None"],
		["OUTLINE"] = L["Outline"],
		["THICKOUTLINE"] = L["Thick outline"],
		["MONOCHROME"] = L["Monochrome"],
	}

	local function GetAnchorFrames(name)
		local anchorFrames = {
			["UIParent"] = L["Screen"],
			["MinimapCluster"] = L["Minimap"],
			["SAF_BuffFrame"] = L["Buffs"],
			["SAF_DebuffFrame"] = L["Debuffs"],
			["SAF_ImbueFrame"] = L["Imbues"],
			["Custom"] = L["Custom frame"],
		}
		anchorFrames["SAF_"..name] = nil -- remove self from selection list so you don't try to anchor the frame to itself
		return anchorFrames
	end

	-- recursive function that goes up the anchor tree until UIParent to check if any are anchored to the frame
	local function CheckRelatives(sourceName, targetFrame)
		if type(targetFrame) == "function" then -- UIParent is anchored to a function
			return true
		elseif targetFrame:GetName() == sourceName then
			return false
		else
			local n = targetFrame:GetNumPoints()
			for i = 1, n do
				local _, relativeTo = targetFrame:GetPoint(i)
				if relativeTo and not CheckRelatives(sourceName, relativeTo) then
					return false
				end
			end
			return true -- all relatives of targetFrame checked
		end
	end

	local function CheckAnchor(sourceName, targetName)
		local targetFrame = getglobal(targetName)
		if targetFrame and targetFrame.IsObjectType and targetFrame:IsObjectType("Region") then -- check if frame exists and can be anchored to
			local sourceFrame = "SAF_"..sourceName
			if targetName ~= sourceFrame then -- check if trying to anchor a frame to itself
				-- check if targetFrame or its relatives are anchored to this frame
				return CheckRelatives(sourceFrame, targetFrame) or L["Target frame is anchored to this frame"]
			else
				return L["Can't anchor a frame to itself"]
			end
		else
			return L["Frame not found"]
		end
	end

	local function GetOptionsTemplate(name, displayName, order)
		return {
			name = displayName,
			type = 'group',
			order = order,
			get = function(key)
				return self.db.profile[name][key]
			end,
			set = function(key, value)
				self.db.profile[name][key] = value
				self:RedrawFrames()
			end,
			pass = true,
			args = {
				header_alignment = {
					type = 'header',
					name = L["Rows and columns"],
					order = 1,
				},
				columnOffset = {
					type = 'range',
					name = L["Column spacing"],
					min = 0,
					max = 50,
					step = 1,
					order = 2,
				},
				rowOffset = {
					type = 'range',
					name = L["Row spacing"],
					min = 0,
					max = 50,
					step = 1,
					order = 3,
				},
				columnDirection = {
					type = 'text',
					name = L["Column direction"],
					validate = {
						["LEFT"] = L["Left"],
						["RIGHT"] = L["Right"],
					},
					order = 4,
				},
				rowDirection = {
					type = 'text',
					name = L["Row direction"],
					validate = {
						["UP"] = L["Up"],
						["DOWN"] = L["Down"],
					},
					order = 5,
				},
				maxColumns = {
					type = 'range',
					name = L["Columns"],
					min = 1,
					max = self.defaults[name].maxButtons,
					step = 1,
					order = 6,
				},
				------------------------------
				header_fonts = {
					type = 'header',
					name = L["Fonts"],
					order = 10,
				},
				durationSize = {
					type = 'range',
					name = L["Duration font size"],
					min = 1,
					max = 50,
					step = 1,
					order = 13,
				},
				stackSize = {
					type = 'range',
					name = L["Stack font size"],
					min = 1,
					max = 50,
					step = 1,
					order = 14,
				},
				durationFlag = {
					type = 'text',
					name = L["Duration font effect"],
					validate = fontEffects,
					order = 15,
				},
				stackFlag = {
					type = 'text',
					name = L["Stack font effect"],
					validate = fontEffects,
					order = 16,
				},
				------------------------------
				header_size = {
					type = 'header',
					name = L["Size and scale"],
					order = 20,
				},
				iconSize = {
					type = 'range',
					name = L["Icon size"],
					min = 1,
					max = 100,
					step = 1,
					order = 21,
				},
				scale = {
					type = 'range',
					name = L["Scale"],
					min = 0.1,
					max = 10,
					step = 0.1,
					order = 22,
				},
				------------------------------
				header_position = {
					type = 'header',
					name = L["Position"],
					order = 30,
				},
				xOffset = {
					type = 'range',
					name = L["Horizontal offset"],
					min = -500,
					max = 500,
					step = 0.1,
					order = 31,
				},
				yOffset = {
					type = 'range',
					name = L["Vertical offset"],
					min = -500,
					max = 500,
					step = 0.1,
					order = 32,
				},
				point = {
					type = 'text',
					name = L["Anchor point"],
					validate = anchorPoints,
					order = 33,
				},
				relativePoint = {
					type = 'text',
					name = L["Relative point"],
					validate = anchorPoints,
					order = 34,
				},
				relativeTo = {
					type = 'text',
					name = L["Anchor to"],
					get = function(key)
						if self.db.profile[name].custom then
							return "Custom"
						else
							return self.db.profile[name].relativeTo
						end
					end,
					set = function(value)
						if value == "Custom" then
							self.db.profile[name].custom = true
							self.db.profile[name].relativeTo = self.db.profile[name].customAnchor
						else
							local canAnchor = CheckAnchor(name, value)
							if canAnchor == true then
								self.db.profile[name].custom = false
								self.db.profile[name].relativeTo = value
							else
								self:Print(canAnchor)
							end
						end
						self:RepositionFrame(name)
					end,
					validate = GetAnchorFrames(name),
					order = 35,
				},
				customFrame = {
					type = 'text',
					name = L["Custom frame"],
					desc = L["Enter the global name of a frame to anchor to"],
					get = function(key)
						return self.db.profile[name].customAnchor
					end,
					set = function(value)
						self.db.profile[name].relativeTo = value
						self.db.profile[name].customAnchor = value
						self:RepositionFrame(name)
					end,
					validate = function(key)
						local canAnchor = CheckAnchor(name, key) 
						if canAnchor == true then
							return true
						else
							self:Print(canAnchor)
							return false
						end
					end,
					disabled = function()
						return not self.db.profile[name].custom
					end,
					order = 36,
				},
			},
		}
	end

	local options = {
		name = "SimpleAuras",
		handler = self,
		type = 'group',
		get = function(key)
			return self.db.profile[key]
		end,
		set = function(key, value)
			self.db.profile[key] = value
			self:RedrawFrames()
		end,
		pass = true,
		args = {
			BuffFrame = GetOptionsTemplate("BuffFrame", L["Buffs"], 10),
			DebuffFrame = GetOptionsTemplate("DebuffFrame", L["Debuffs"], 20),
			ImbueFrame = GetOptionsTemplate("ImbueFrame", L["Imbues"], 30),
			lock = {
				type = 'toggle',
				name = L["Unlock frames"],
				get = function() return self.unlocked end,
				set = function(value)
					if value then
						self:Print(L["Frames unlocked"])
					else
						self:Print(L["Frames locked"])
					end
					self:LockFrames()
				end,
				order = 1,
			},
			timeFormat = {
				type = 'text',
				name = L["Time format"],
				validate = {
					["1"] = L["Blizzlike"],
					["2"] = L["Detailed"],
				},
				order = 2,
			},
			thickBorders = {
				type = 'toggle',
				name = L["Thick borders"],
				order = 3,
			},
		},
	}
	local profileoptions = AceLibrary("AceDB-2.0"):GetAceOptionsDataTable(self)
	options.args.profile = profileoptions.profile
	options.args.profile.args.resetConfig = {
		type = 'execute',
		name = L["Reset profile"],
		desc = L["Revert current profile settings to default"],
		func = function()
			self:ResetDB("profile")
			self:OnProfileEnable()
			self:Print(L["Profile reset"])
		end,
	}

	return options
end

function SimpleAuras:ShowOptions()
	if self.GetOptions then
		waterfall:Register("SimpleAuras", "aceOptions", self:GetOptions())
		self.GetOptions = nil
	end
	waterfall:Open("SimpleAuras")
end