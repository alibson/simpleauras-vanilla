local L = AceLibrary("AceLocale-2.2"):new("SimpleAuras")

L:RegisterTranslations("enUS", function() return {
-- Anchor points
["Top right"] = true,
["Top left"] = true,
["Bottom right"] = true,
["Bottom left"] = true,
["Top"] = true,
["Bottom"] = true,
["Right"] = true,
["Left"] = true,
["Center"] = true,

-- Font effects
["None"] = true,
["Outline"] = true,
["Thick outline"] = true,
["Monochrome"] = true,

-- Anchor frames
["Screen"] = true,
["Minimap"] = true,
["Buffs"] = true,
["Debuffs"] = true,
["Imbues"] = true,
["Custom frame"] = true,

-- Grow directions
["Up"] = true,
["Down"] = true,

-- Time format
["Time format"] = true,
["Blizzlike"] = true,
["Detailed"] = true,

-- Borders
["Thick borders"] = true,

-- Config
["Rows and columns"] = true,
	["Column spacing"] = true,
	["Row spacing"] = true,
	["Column direction"] = true,
	["Row direction"] = true,
	["Columns"] = true,
["Fonts"] = true,
	["Duration font size"] = true,
	["Stack font size"] = true,
	["Duration font effect"] = true,
	["Stack font effect"] = true,
["Size and scale"] = true,
	["Icon size"] = true,
	["Scale"] = true,
["Position"] = true,
	["Horizontal offset"] = true,
	["Vertical offset"] = true,
	["Anchor point"] = true,
	["Relative point"] = true,
	["Anchor to"] = true,
	["Enter the global name of a frame to anchor to"] = true,
		["Frame not found"] = true,
		["Can't anchor a frame to itself"] = true,
		["Target frame is anchored to this frame"] = true,

["Unlock frames"] = true,
["Frames locked"] = true,
["Frames unlocked"] = true,

["Reset profile"] = true,
["Revert current profile settings to default"] = true,
["Profile reset"] = true,

} end)
