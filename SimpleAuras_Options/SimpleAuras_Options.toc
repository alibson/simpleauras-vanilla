## Interface: 11200
## Title: SimpleAuras Options|cff7fff7f -Ace2-|r
## Version: 1.0
## Author: Alibson 
## Notes: SimpleAuras load on demand options menu
## LoadOnDemand: 1
## Dependencies: SimpleAuras

Libs\AceLocale-2.2\AceLocale-2.2.lua
Libs\Waterfall-1.0\Waterfall-1.0.lua

Locales\Locales.xml
Options.lua
