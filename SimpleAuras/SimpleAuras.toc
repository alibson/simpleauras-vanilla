## Interface: 11200
## Title: SimpleAuras|cff7fff7f -Ace2-|r
## Version: 1.0
## Author: Alibson 
## Notes: Just a simple buff display
## SavedVariables: SimpleAurasDB
## OptionalDeps: Ace2
## X-Embeds: Ace2
## X-Category: Buffs

#libs
Libs\AceLibrary\AceLibrary.lua
Libs\AceOO-2.0\AceOO-2.0.lua

Libs\AceEvent-2.0\AceEvent-2.0.lua
Libs\AceDB-2.0\AceDB-2.0.lua
Libs\AceConsole-2.0\AceConsole-2.0.lua
Libs\AceAddon-2.0\AceAddon-2.0.lua

#addon
Core.lua
Templates.xml
