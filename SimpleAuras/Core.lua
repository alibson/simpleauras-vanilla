-------------------------------------------------------------------------------
-- Create addon
-------------------------------------------------------------------------------

SimpleAuras = AceLibrary("AceAddon-2.0"):new("AceConsole-2.0", "AceEvent-2.0", "AceDB-2.0")

-------------------------------------------------------------------------------
-- Locals
-------------------------------------------------------------------------------

local DebuffTypeColor = DebuffTypeColor
local BUFF_WARNING_TIME = BUFF_WARNING_TIME -- 31s by default, makes buttons flash
local BUFF_DURATION_WARNING_TIME = BUFF_DURATION_WARNING_TIME -- 60s by default, changes duration text color
local HIGHLIGHT_FONT_COLOR = HIGHLIGHT_FONT_COLOR
local NORMAL_FONT_COLOR = NORMAL_FONT_COLOR

local GetPlayerBuff = GetPlayerBuff
local GetPlayerBuffTexture = GetPlayerBuffTexture
local GetPlayerBuffApplications = GetPlayerBuffApplications
local GetPlayerBuffDispelType = GetPlayerBuffDispelType
local GetPlayerBuffTimeLeft = GetPlayerBuffTimeLeft
local GetWeaponEnchantInfo = GetWeaponEnchantInfo
local GetInventoryItemTexture = GetInventoryItemTexture
local CreateFrame = CreateFrame

local ceil = ceil
local mod = mod
local format = format

local function GetRandomArgument(t)
	return t[random(table.getn(t))]
end

local alphaValue = 1
local maxButtons = {}

local frames = {"BuffFrame", "DebuffFrame", "ImbueFrame"}

-------------------------------------------------------------------------------
-- Format time
-------------------------------------------------------------------------------

local FormatTime

local DAY_ONELETTER_ABBR = DAY_ONELETTER_ABBR
local HOUR_ONELETTER_ABBR = HOUR_ONELETTER_ABBR
local MINUTE_ONELETTER_ABBR = MINUTE_ONELETTER_ABBR
local SECOND_ONELETTER_ABBR = SECOND_ONELETTER_ABBR
local formatFunctions = {
	[1] = function(sec) -- blizzlike
		if sec >= 86400 then
			return format(DAY_ONELETTER_ABBR, sec / 86400 + 1) -- blizz always rounds up
		elseif sec >= 3600 then
			return format(HOUR_ONELETTER_ABBR, sec / 3600 + 1)
		elseif sec >= 60 then
			return format(MINUTE_ONELETTER_ABBR, sec / 60 + 1)
		else
			return format(SECOND_ONELETTER_ABBR, sec)
		end
	end,
	[2] = function(sec) -- detailed
		if sec < 60 then
			return format("%d", sec)
		else
			return format("%d:%02d", sec / 60, mod(sec, 60))
		end
	end,
}

function SimpleAuras:SetTimeFormat()
	FormatTime = formatFunctions[tonumber(self.db.profile.timeFormat)]
end

-------------------------------------------------------------------------------
-- Config defaults
-------------------------------------------------------------------------------

SimpleAuras.defaults = {
	thickBorders = true,
	timeFormat = "2",

	BuffFrame = {
		maxButtons = 32,
		maxColumns = 16,
		rowDirection = "DOWN",

		point = "TOPRIGHT",
		relativeTo = "UIParent",
		relativePoint = "TOPRIGHT",
		xOffset = -220.5,
		yOffset = -10.5,
		scale = 1,
	},
	DebuffFrame = {
		maxButtons = 16,
		maxColumns = 8,
		rowDirection = "DOWN",

		point = "TOPRIGHT",
		relativeTo = "SAF_BuffFrame",
		relativePoint = "BOTTOMRIGHT",
		xOffset = 0,
		yOffset = -5,
		scale = 2,
	},
	ImbueFrame = {
		maxButtons = 2,
		maxColumns = 1,
		rowDirection = "UP",

		point = "TOPLEFT",
		relativeTo = "SAF_BuffFrame",
		relativePoint = "TOPRIGHT",
		xOffset = 5,
		yOffset = 0,
		scale = 1,
	},
}

for i = 1, 3 do -- fill out shared defaults
	local t = SimpleAuras.defaults[frames[i]]
	t.iconSize = 30
	t.durationSize = 10
	t.durationFlag = "none"
	t.stackSize = 14
	t.stackFlag = "OUTLINE"
	t.rowOffset = 15
	t.columnOffset = 5
	t.columnDirection = "LEFT"
	t.custom = false
	t.customAnchor = "UIParent"
end

-------------------------------------------------------------------------------
-- Initialization
-------------------------------------------------------------------------------

function SimpleAuras:OnInitialize()
	-- AceDB-2.0
	self:RegisterDB("SimpleAurasDB")
	self:RegisterDefaults("profile", self.defaults)
	-- Options
	self:RegisterChatCommand({ "/simpleauras", "/sa" }, function()
		if not self.ShowOptions then
			local name = "SimpleAuras_Options"
			if not IsAddOnLoaded(name) then
				EnableAddOn(name)
				local loaded, reason = LoadAddOn(name)
				if not loaded then
					self:Print("Error loading options:", reason)
					return
				end
			end
		end
		self:ShowOptions()
	end)
	-- create frames
	self:InitFrames()
end

function SimpleAuras:OnEnable()
	self:RegisterEvent("PLAYER_AURAS_CHANGED")
	self:RegisterEvent("UNIT_INVENTORY_CHANGED")

	self.FlashFrame:SetScript("OnUpdate", function() self:FlashFrameOnUpdate() end)
end

function SimpleAuras:OnProfileEnable()
	self:RedrawFrames()
end

-------------------------------------------------------------------------------
-- Event handlers
-------------------------------------------------------------------------------

function SimpleAuras:PLAYER_AURAS_CHANGED()
	-- handle buffs
	local count = 0
	for index = 1, maxButtons["BuffFrame"] do
		if self:SetButton(index, "HELPFUL") then
			count = count + 1
		end
	end
	self.BuffFrame:SetID(count) -- frame ID stores number of buttons shown

	-- handle debuffs
	count = 0
	for index = 1, maxButtons["DebuffFrame"] do
		if self:SetButton(index, "HARMFUL") then
			count = count + 1
		end
	end
	self.DebuffFrame:SetID(count) -- frame ID stores number of buttons shown
end

function SimpleAuras:UNIT_INVENTORY_CHANGED(event, unit)
	if unit == "player" then
		self:ScanImbues()
	end
end

-------------------------------------------------------------------------------
-- Buff update
-------------------------------------------------------------------------------

function SimpleAuras:SetButton(index, filter)
	local button = filter == "HELPFUL" and self.BuffFrame.buttons[index] or self.DebuffFrame.buttons[index]
	local buffIndex, untilCancelled = GetPlayerBuff(index-1, filter)

	if buffIndex < 0 then -- no buff
		button:Hide()
		return false
	else -- has buff
		-- set ID
		button:SetID(buffIndex)
		-- set texture
		button.icon:SetTexture(GetPlayerBuffTexture(buffIndex))
		-- set stacks
		local count = GetPlayerBuffApplications(buffIndex)
		button.count:SetText(count > 1 and count or "")
		-- set border color
		if filter == "HARMFUL" then -- is debuff
			local color = DebuffTypeColor[GetPlayerBuffDispelType(buffIndex) or "none"]
			self:SetBorderColor(button, color.r, color.g, color.b)
		end
		-- show button
		button:Show()
		-- set duration
		button.untilCancelled = untilCancelled
		self:SetAuraDuration(button, untilCancelled == 0 and GetPlayerBuffTimeLeft(buffIndex) or 0)
		return true
	end
end

function SimpleAuras:SetBorderColor(button, r, g, b)
	if self.db.profile.thickBorders then
		button:SetBackdropBorderColor(r, g, b)
	else
		button:SetBackdropColor(r, g, b)
	end
end

function SimpleAuras:UpdateAuraDurations(frame)
	local button
	for i = 1, frame:GetID() do -- does nothing if ID < 1
		button = frame.buttons[i]
		if button.untilCancelled == 0 then -- don't update text for auras with no duration
			self:SetAuraDuration(button, GetPlayerBuffTimeLeft(button:GetID()))
		end
	end
end

function SimpleAuras:SetAuraDuration(button, duration)
	if duration == 0 then -- no duration
		button.duration:SetText("")
	else -- has duration
		button.duration:SetText(FormatTime(duration))
		if duration < BUFF_DURATION_WARNING_TIME then
			button.duration:SetVertexColor(HIGHLIGHT_FONT_COLOR.r, HIGHLIGHT_FONT_COLOR.g, HIGHLIGHT_FONT_COLOR.b)
			if duration < BUFF_WARNING_TIME then -- enable flashing
				button.flashing = true
				return
			end
		else
			button.duration:SetVertexColor(NORMAL_FONT_COLOR.r, NORMAL_FONT_COLOR.g, NORMAL_FONT_COLOR.b)
		end
	end
	button.flashing = false
	button:SetAlpha(1)
end

-------------------------------------------------------------------------------
-- Imbues
-------------------------------------------------------------------------------

function SimpleAuras:ScanImbues()
	local hasMainHandEnchant, mainHandExpiration, mainHandCharges, hasOffHandEnchant, offHandExpiration, offHandCharges = GetWeaponEnchantInfo()

	if hasOffHandEnchant then
		local button = self.ImbueFrame.buttons[1]
		button.icon:SetTexture(GetInventoryItemTexture("player", 17)) -- set texture
		button.count:SetText(offHandCharges > 1 and offHandCharges or "") -- set stacks
		button:Show() -- show button
		self:SetAuraDuration(button, offHandExpiration/1000) -- set duration
		--self:SetAuraDuration(button, offHandExpiration and offHandExpiration/1000 or nil) -- are there any imbues with nil duration?
	else
		self.ImbueFrame.buttons[1]:Hide()
	end

	if hasMainHandEnchant then
		local button = self.ImbueFrame.buttons[2]
		button.icon:SetTexture(GetInventoryItemTexture("player", 16)) -- set texture
		button.count:SetText(mainHandCharges > 1 and mainHandCharges or "") -- set stacks
		button:Show() -- show button
		self:SetAuraDuration(button, mainHandExpiration/1000) -- set duration
		-- self:SetAuraDuration(button, mainHandExpiration and mainHandExpiration/1000 or nil) -- are there any imbues with nil duration?
	else
		self.ImbueFrame.buttons[2]:Hide()
	end
end

-------------------------------------------------------------------------------
-- Scripts
-------------------------------------------------------------------------------

do
	local flashTime = 0
	local fading = false
	local durationUpdateTimer = 0

	function SimpleAuras:FlashFrameOnUpdate()
		local arg1 = arg1
		-- update alphaValue
		flashTime = flashTime - arg1
		if flashTime < 0 then
			local overtime = -flashTime
			fading = not fading
			flashTime = 0.75
			if overtime < flashTime then
				flashTime = flashTime - overtime
			end
		end
		if not fading then
			alphaValue = (0.75 - flashTime) / 0.75
		else
			alphaValue = flashTime / 0.75
		end
		alphaValue = (alphaValue * (1 - 0.3)) + 0.3

		-- update durationUpdateTimer
		durationUpdateTimer = durationUpdateTimer + arg1
		if durationUpdateTimer > 1 then
			self:UpdateAuraDurations(self.BuffFrame)
			self:UpdateAuraDurations(self.DebuffFrame)
			self:ScanImbues()
			durationUpdateTimer = 0
		end
	end
end

function SimpleAuras:FlashButton(button)
	if button.flashing then
		button:SetAlpha(alphaValue)
	end
end

function SimpleAuras:FinishedMoving(this)
	this:StopMovingOrSizing()
	local db = self.db.profile[string.sub(this:GetName(), 5)]
	local point, _, relativePoint, xOffset, yOffset = this:GetPoint()
	db.custom = false -- disable custom anchor
	db.point = point
	db.relativeTo = "UIParent"
	db.relativePoint = relativePoint
	db.xOffset = xOffset
	db.yOffset = yOffset
end

-------------------------------------------------------------------------------
-- Align functions
-------------------------------------------------------------------------------

function SimpleAuras:AlignButtons(frameName)
	local buttons = self[frameName].buttons
	local point, relativeTo, relativePoint, xOffset, yOffset

	local db = self.db.profile[frameName]
	local maxColumns = db.maxColumns
	local columnDirection = db.columnDirection
	local rowDirection = db.rowDirection
	local columnOffset = db.columnOffset
	local rowOffset = db.rowOffset

	-- first button
	buttons[1]:ClearAllPoints()
	if rowDirection == "DOWN" then
		if columnDirection == "LEFT" then
			point = "TOPRIGHT";		relativePoint = "TOPRIGHT";		xOffset = -2;	yOffset = -2
		else -- go right
			point = "TOPLEFT";		relativePoint = "TOPLEFT";		xOffset = 2;	yOffset = -2
		end
	else -- go up
		local _, fontSize = buttons[1].duration:GetFont()
		if columnDirection == "LEFT" then
			point = "BOTTOMRIGHT";	relativePoint = "BOTTOMRIGHT";	xOffset = -2;	yOffset = 2 + fontSize
		else -- go right
			point = "BOTTOMLEFT";	relativePoint = "BOTTOMLEFT";	xOffset = 2;	yOffset = 2 + fontSize
		end
	end
	buttons[1]:SetPoint(point, self[frameName], relativePoint, xOffset, yOffset)

	local column = 1

	for i = 2, maxButtons[frameName] do
		-- find anchor
		if column >= maxColumns then -- start a new row
			relativeTo = buttons[i - maxColumns] -- align to first button of previous row
			if rowDirection == "DOWN" then
				point = "TOPRIGHT";		relativePoint = "BOTTOMRIGHT";	yOffset = -rowOffset
			else -- go up
				point = "BOTTOMRIGHT";	relativePoint = "TOPRIGHT";		yOffset = rowOffset
			end
			xOffset = 0
			column = 0 -- reset column counter
		else -- continue previous row
			relativeTo = buttons[i - 1]
			if columnDirection == "LEFT" then
				point = "TOPRIGHT";		relativePoint = "TOPLEFT";		xOffset = -columnOffset
			else -- go right
				point = "TOPLEFT";		relativePoint = "TOPRIGHT";		xOffset = columnOffset
			end
			yOffset = 0
		end
		-- SetPoint
		buttons[i]:ClearAllPoints()
		buttons[i]:SetPoint(point, relativeTo, relativePoint, xOffset, yOffset)
		column = column + 1
	end
end

-------------------------------------------------------------------------------
-- Frames
-------------------------------------------------------------------------------

function SimpleAuras:CreateButton(parent, index)
	local name = parent:GetName()
	local template = name == "SAF_ImbueFrame" and "SAFImbueButtonTemplate" or "SAFButtonTemplate"
	name = format("%s_Button%d", name, index) -- "SAF_BuffFrame_Button1"
	parent.buttons[index] = CreateFrame("Button", name, parent, template)
	parent.buttons[index]:SetScript("OnUpdate", function() self:FlashButton(parent.buttons[index]) end)
end

function SimpleAuras:InitFrames()
	-- hide blizz frames
	BuffFrame:UnregisterAllEvents()
	BuffFrame:Hide()
	TemporaryEnchantFrame:UnregisterAllEvents()
	TemporaryEnchantFrame:Hide()

	-- set FormatTime function
	self:SetTimeFormat()

	-- create frame and its buttons
	local frameName, frame
	for i = 1, 3 do
		frameName = frames[i]
		-- create frame
		self[frameName] = CreateFrame("Frame", "SAF_"..frameName, UIParent, "SAFBaseFrameTemplate")
		frame = self[frameName]
		-- create buttons
		frame.buttons = {}
		maxButtons[frameName] = self.db.profile[frameName].maxButtons
		for i = 1, maxButtons[frameName] do
			self:CreateButton(frame, i)
		end
		-- touch up frames
		frame:SetScale(self.db.profile[frameName].scale)
		self:StyleButtons(frameName)
		self:AlignButtons(frameName)
		self:ResizeFrame(frameName)
	end

	-- position frames after creating all of them, in case they're anchored to each other
	for i = 1, 3 do
		self:RepositionFrame(frames[i])
	end

	-- assign IDs to imbue buttons
	self.ImbueFrame.buttons[1]:SetID(17) -- off hand
	self.ImbueFrame.buttons[2]:SetID(16) -- main hand

	-- create FlashFrame
	self.FlashFrame = CreateFrame("Frame", "SAF_FlashFrame", self.BuffFrame)
end

function SimpleAuras:ResizeFrame(frameName)	-- resizes frame to fit its buttons
	local db = self.db.profile[frameName]
	local _, fontSize = self[frameName].buttons[1].duration:GetFont()
	self[frameName]:SetHeight(((db.iconSize + db.rowOffset) * ceil(maxButtons[frameName] / db.maxColumns)) - db.rowOffset + 4 + fontSize) -- (button+offset) * rowCount - offset + border + durationText
	self[frameName]:SetWidth((db.iconSize + db.columnOffset) * db.maxColumns - db.columnOffset + 4) -- (button+offset) * maxColumns - offset + border
end

function SimpleAuras:RepositionFrame(frameName)
	local db = self.db.profile[frameName]
	self[frameName]:ClearAllPoints()
	self[frameName]:SetPoint(db.point, db.relativeTo, db.relativePoint, db.xOffset, db.yOffset)
end

function SimpleAuras:StyleButtons(frameName)
	local db = self.db.profile[frameName]
	local thickBorders = self.db.profile.thickBorders
	local iconSize = db.iconSize
	local durationFont = GameFontNormalSmall:GetFont()
	local durationSize = db.durationSize
	local durationFlag = db.durationFlag
	local stackFont = NumberFontNormal:GetFont()
	local stackSize = db.stackSize
	local stackFlag = db.stackFlag
	local button

	for i = 1, maxButtons[frameName] do
		button = self[frameName].buttons[i]
		-- set size
		button:SetHeight(iconSize)
		button:SetWidth(iconSize)
		-- set fonts
		button.duration:SetFont(durationFont, durationSize, durationFlag)
		button.duration:SetWidth(iconSize*2) -- needs to be set or the text will cut off
		button.count:SetFont(stackFont, stackSize, stackFlag)
		-- set border
		button:SetBackdropColor(0, 0, 0)
		if thickBorders then
			button.icon:SetPoint("TOPLEFT", 2, -2)
			button.icon:SetPoint("BOTTOMRIGHT", -2, 2)
			if frameName == "ImbueFrame" then
				button:SetBackdropBorderColor(.4, .2, .6)
			else
				button:SetBackdropBorderColor(0.5, 0.5, 0.5, 1)
			end
		else
			button.icon:SetPoint("TOPLEFT", 0, 0)
			button.icon:SetPoint("BOTTOMRIGHT", 0, 0)
			button:SetBackdropBorderColor(0, 0, 0, 0)
			if frameName == "ImbueFrame" then
				button:SetBackdropColor(.4, .2, .6)
			end
		end
	end
end

function SimpleAuras:RedrawFrames()
	self:SetTimeFormat()
	
	local frameName
	for i = 1, 3 do
		frameName = frames[i]
		self[frameName]:SetScale(self.db.profile[frameName].scale)
		self:RepositionFrame(frameName)
		self:ResizeFrame(frameName)
		self:AlignButtons(frameName)
		self:StyleButtons(frameName)
	end

	if not self.unlocked then -- trigger buff update if frames are locked
		self:PLAYER_AURAS_CHANGED()
		self:ScanImbues()
	else 
		local color
		for i = 1, 3 do
			frameName = frames[i]
			for i = 1, maxButtons[frameName] do
				self[frameName].buttons[i].duration:SetText(FormatTime(random(3600))) -- redraw dummy duration
				if frameName == "DebuffFrame" then -- redraw dummy debuff borders
					color = DebuffTypeColor[GetRandomArgument({"none", "Magic", "Curse", "Disease", "Poison"})]
					self:SetBorderColor(self.DebuffFrame.buttons[i], color.r, color.g, color.b)
				end
			end
		end
	end
end

function SimpleAuras:LockFrames()
	if self.unlocked then -- lock frames
		local frame, button
		for i = 1, 3 do
			frame = self[frames[i]]
			-- disable moving
			frame:EnableMouse(false)
			-- hide backdrop
			frame:SetBackdropColor(0, 0, 0, 0)
			-- reset buttons
			for i = 1, maxButtons[frames[i]] do
				button = frame.buttons[i]
				-- reset stack text
				button.count:SetText("")
				-- enable mouse on buttons
				button:EnableMouse(true)
			end
		end

		-- trigger update
		self:PLAYER_AURAS_CHANGED()
		self:ScanImbues()

		-- enable updates
		self:OnEnable()

		self.unlocked = false
	else -- unlock frames
		local frameColors = { {0,1,0}, {1,0,0}, {0,0,1} }

		-- disable updates
		self:UnregisterAllEvents()
		self.FlashFrame:SetScript("OnUpdate", nil)

		local frame, button, color
		for i = 1, 3 do
			frame = self[frames[i]]
			-- make frame movable
			frame:EnableMouse(true)
			-- show backdrop
			frame:SetBackdropColor(frameColors[i][1], frameColors[i][2], frameColors[i][3], .3)

			-- show dummy icons
			for i = 1, maxButtons[frames[i]] do
				button = frame.buttons[i]
				-- set texture
				button.icon:SetTexture([[Interface\Icons\Temp]])
				-- set border color if debuff
				if frame == self.DebuffFrame then
					color = DebuffTypeColor[GetRandomArgument({"none", "Magic", "Curse", "Disease", "Poison"})]
					self:SetBorderColor(button, color.r, color.g, color.b)
				end
				-- set duration text
				button.duration:SetVertexColor(NORMAL_FONT_COLOR.r, NORMAL_FONT_COLOR.g, NORMAL_FONT_COLOR.b)
				button.duration:SetText(FormatTime(random(3600)))
				-- set stack text
				button.count:SetText(i)
				-- disable flashing
				button.flashing = false
				button:SetAlpha(1)
				-- disable mouse
				button:EnableMouse(false)
				-- show icon
				button:Show()
			end
		end

		-- special imbue labels
		self.ImbueFrame.buttons[1].count:SetText("OH")
		self.ImbueFrame.buttons[2].count:SetText("MH")
		
		self.unlocked = true
	end
end
